package org.roshini.words;

import java.time.Year;

// Written by Roshini Saravanakumar 
// API  
// User(String username, int birthYear)       // Create a new user  
// public String getUsername()                // Get the user's username   
// public int getBirthYear()                  // Get the user's birth year  
// public Iterable<User> getFriends()         // Return an alphabetized set with the user's friends 
// public void addFriend(User newFriend)      // Add a friend to the user's records 

public class User {
	private final String username;
	private final Year birthYear;
	private AlphabetizedSet<User> friends;
	private static final int MIN_USERNAME_LENGTH = 4;
	private static final int MAX_USERNAME_LENGTH = 17;

	// Constructor to create a new user
	User(String username, Year birthYear) {
		// If the username's length is out of range, throw an exception
		if (username.length() < MIN_USERNAME_LENGTH || username.length() > MAX_USERNAME_LENGTH) {
			throw new java.lang.IllegalArgumentException("Username length should be between " + MIN_USERNAME_LENGTH + " and " + MAX_USERNAME_LENGTH + " characters!");
		}   
		 
		char firstCharacterInUsername = username.charAt(0);  
		String specialCharacters = "~`!1@2#3$4%5^6&7*8(9)0_-+= {[}]|\\:;'<,>.?/";
		for(char specialCharacter: specialCharacters.toCharArray()) { 
			if(firstCharacterInUsername == specialCharacter || firstCharacterInUsername == '"') { 
				throw new java.lang.IllegalArgumentException("The first character in the username cannot be a special character!");
			}
		} 
		
		// If the birth year is invalid, throw an exception
		if (birthYear.isBefore(Year.of(1900)) || birthYear.isAfter(Year.now())) {
			throw new java.lang.IllegalArgumentException("Invalid birth year!");
		}

		this.username = username;
		this.birthYear = birthYear;
		this.friends = new AlphabetizedSet<User>();
	}

	// Return the user's username
	public String getUsername() {
		return this.username;
	}

	// Return the user's birth year
	public Year getBirthYear() {
		return this.birthYear;
	}

	// Return an alphabetized stack with the user's friends
	public Iterable<User> getFriends() {
		return this.friends.alphabetizedStack();
	} 
	 
	public int getFriendCount() { 
		return this.friends.size(); 
	}

	// Add a friend to the user's records
	public void addFriend(User newFriend) {
		this.friends.insert(newFriend.username, newFriend);
	}

	// Remove a friend from the user's records
	// Will be implemented soon...
	// public void removeFriend(User friend)  
	
} 
