package org.roshini.words;

import static org.junit.jupiter.api.Assertions.*;
import java.time.Year;
import org.junit.jupiter.api.Test;

class UserTest {

	@Test
	// Assert that the constructor for user throws the right exception when the
	// constructor is called with an invalid birth year
	void invalidBirthYearThrowsExceptionTest() {
		assertThrows(IllegalArgumentException.class, () -> new User("before1900", Year.of(1899)));
		assertThrows(IllegalArgumentException.class, () -> new User("afterCurrentYear", Year.of(2019)));
	}

	@Test
	// Assert that the constructor for user throws the right exception when the
	// constructor is called with a username whose length is out of bounds
	void outOfBoundsUsernameLengthThrowsExceptionTest() {
		assertThrows(IllegalArgumentException.class, () -> new User("hi", Year.of(2000)));
		assertThrows(IllegalArgumentException.class,
				() -> new User("ashfdkhaksjdgfshdjfgsdhfksdgfhdsjk", Year.of(2000)));
	}

	@Test
	// Check that the constructor for user throws the right exception when the
	// constructor is called with a username that starts with a special character or
	// number
	void testFirstCharacterAsInvalidSpecialCharacterUsername() {
		assertThrows(IllegalArgumentException.class, () -> new User("@hello", Year.of(2000)));
	}

	@Test
	// Check that getUsername() returns the right user name
	void getsRightUsername() {
		User test = new User("testUser7", Year.of(2000));
		assertEquals("testUser7", test.getUsername());
	}

	@Test
	// Check that getBirthYear() returns the right birth year
	void getsRightBirthYear() {
		User test = new User("testUser7", Year.of(2000));
		assertEquals(Year.of(2000), test.getBirthYear());
	}

	@Test
	void addsAllFriendsToTheFriendList() {
		User test = new User("test", Year.of(2000));
		test.addFriend(new User("beta", Year.of(2000)));
		test.addFriend(new User("apple", Year.of(2000)));
		test.addFriend(new User("Zebra", Year.of(2000)));
		test.addFriend(new User("Hello", Year.of(2000)));
		test.addFriend(new User("Alpha", Year.of(1992)));
		test.addFriend(new User("mang", Year.of(1994)));

		assertEquals(6, test.getFriendCount());

	}

	@Test
	// Check that getFriends() returns the correctly alphabetized list of friends
	void getsCorrectlyAlphabetizedFriendList() {
		String[] friendList = {"Alpha", "apple", "beta", "Hello", "mang", "Zebra" };
		User test = new User("test", Year.of(2000));
		test.addFriend(new User("beta", Year.of(2000)));
		test.addFriend(new User("apple", Year.of(2000)));
		test.addFriend(new User("Zebra", Year.of(2000)));
		test.addFriend(new User("Hello", Year.of(2000)));
		test.addFriend(new User("Alpha", Year.of(1992)));
		test.addFriend(new User("mang", Year.of(1994)));
		int i = 0; 
		
		for (User u : test.getFriends()) {
			assertEquals(friendList[i++], u.getUsername());
		}
	} 
	
}
