package org.roshini.words;

import java.util.Stack;

// Written by Roshini Saravanakumar 
// NOTE: This set is implemented using the Left-leaning Red Black BST invented by Robert Sedgewick in 2008  

// API  
// AlphabetizedSet()                                // Construct an empty alphabetized set  
// public void insert(String newKey, Item newValue) // Insert an item into the set if it isn't already in the set 
// public boolean contains(String key)              // Return whether or not the set already contains a key 
// public int size()                                // Return the number of items in the set
// public boolean isEmpty()                         // Return whether or not the set is empty   
// public Iterable<Item> alphabetizedStack()        // Return a stack of the items in alphabetic order 

public class AlphabetizedSet<Item> {
	private static final boolean RED = true;
	private static final boolean BLACK = false;
	private Node root;
	private int numberOfItems;

	private class Node {
		String key;
		Item value;
		Node leftChild;
		Node rightChild;
		boolean color;

		// Node constructor
		Node(String key, Item value, boolean color) {
			this.key = key; 

			// Pair the value with the key
			this.value = value;

			// Represents the color of the link connecting this node to its parent node
			this.color = color;
		}
	}

	// Create an empty alphabetized set
	AlphabetizedSet() {

	}

	// The following 4 functions are helper functions to implement the left-leaning
	// red black bst
	// Return true if the color of the node's link to its parent is red
	private boolean isRed(Node node) {
		// If the node is null, return false since there is no red link
		if (node == null) {
			return false;
		}

		return node.color == RED;
	}

	// Perform a left rotation on a node who's right child has a red link
	private Node leftRotation(Node parentNode) {
		// The incorrectly right leaning red linked node is the node's right child
		Node rightLeaningRed = parentNode.rightChild;

		// The in between node that contains values in the range (node < in between <
		// rightLeaningRed) is the right leaning red linked node's left child.
		// Move this in between node to the right child of the parent node
		parentNode.rightChild = rightLeaningRed.leftChild;

		// Rotate the tree so that the parent node becomes the child of the previously
		// incorrect red linked node
		rightLeaningRed.leftChild = parentNode;

		// Swap the color links of the nodes
		rightLeaningRed.color = parentNode.color;
		parentNode.color = RED;

		// Return the new parent node
		return rightLeaningRed;
	}

	// Perform a right rotation so that a left leaning link temporarily leans right
	private Node rightRotation(Node parentNode) {
		// The left leaning red linked node is the parent's left child
		Node leftLeaningRed = parentNode.leftChild;

		// The in between node that contains values in the range (leftLeaningRed < in
		// between <
		// node) is the red linked node's right child.
		// Move this in between node to the left child of the parent node
		parentNode.leftChild = leftLeaningRed.rightChild;

		// Rotate the tree so that the parent node becomes the right child
		// of the left leaning red linked node
		leftLeaningRed.rightChild = parentNode;

		// Swap the color links of the nodes
		leftLeaningRed.color = parentNode.color;
		parentNode.color = RED;

		// Return the new parent node
		return leftLeaningRed;
	}

	// Perform a color flip to split a temporary 4-node.
	// A 4-node is a node with 2 red links coming out of it
	private void colorFlip(Node parentNode) {
		parentNode.color = RED;
		parentNode.leftChild.color = BLACK;
		parentNode.rightChild.color = BLACK;
	}

	// Insert an item into the set if it isn't already in the set
	public void insert(String newKey, Item newValue) {
		// If either of the arguments do not exist, throw an exception
		if (newValue == null || newKey == null) {
			throw new java.lang.IllegalArgumentException();
		}
 
		// Recursively insert the item into the set
		this.root = insert(newKey.toLowerCase(), newValue, this.root);

		// Increment the number of items
		this.numberOfItems++;
	}

	// Recursive helper function to insert a node in the correct spot
	private Node insert(String newKey, Item newValue, Node currentNode) {
		// If the current node is null, create a new node with the inserted item and
		// return it
		if (currentNode == null) {
			return new Node(newKey, newValue, RED);
		}

		// If the string is less than the current node's string, insert it in the left
		// child
		else if (newKey.compareTo(currentNode.key) < 0) {
			currentNode.leftChild = insert(newKey, newValue, currentNode.leftChild);
		}   
		
		// If the string is greater than the current node's string, insert it in the
		// right child
		else if (newKey.compareTo(currentNode.key) > 0) {
			currentNode.rightChild = insert(newKey, newValue, currentNode.rightChild);
		}   
		
		// If the key is already in the set, throw an exception 
		else { 
			throw new java.lang.IllegalArgumentException("This key is already in the set."); 
		}

		// Perform any necessary operations on the BST to maintain order and symmetry

		// If the right child of the node is red and the left child isn't,
		// perform a left rotation on the current node
		if (isRed(currentNode.rightChild) && !isRed(currentNode.leftChild)) {
			currentNode = leftRotation(currentNode);
		}

		// If there are 2 left leaning red links in a row, perform a temporary right
		// rotation on the current node
		if (isRed(currentNode.leftChild) && isRed(currentNode.leftChild.leftChild)) {
			currentNode = rightRotation(currentNode);
		}

		// If both children of the node are red links, this is a temporary 4-node
		// so perform a color flip to split the 4-node
		if (isRed(currentNode.leftChild) && isRed(currentNode.rightChild)) {
			colorFlip(currentNode);
		}

		// Return the current node
		return currentNode;
	}

	// Return whether or not the set already contains the key
	public boolean contains(String key) {
		// If the set is empty, it does not contain the key
		if (this.isEmpty()) {
			return false;
		}

		// Recursively search the tree to determine if it contains the key
		return contains(key.toLowerCase(), this.root);
	}

	// Recursive helper method to search the set for the key
	private boolean contains(String key, Node currentNode) {
		// If the current node is null, the key is not here
		if (currentNode == null) {
			return false;
		}

		// If we have a search hit, the set contains the key
		else if (key.compareTo(currentNode.key) == 0) {
			return true;
		}

		// If the key is less than the current node's key, search the left child
		else if (key.compareTo(currentNode.key) < 0) {
			return contains(key, currentNode.leftChild);
		}

		// Otherwise if the key is greater than the current node's key, search the right
		// child
		else {
			return contains(key, currentNode.rightChild);
		}
	}

	// Remove an item from the set
	// Will be implemented soon...

	// Return the number of items in the set
	public int size() {
		return this.numberOfItems;
	}

	// Return whether or not the set is empty
	public boolean isEmpty() {
		return this.numberOfItems == 0;
	}

	// Return a stack of the items in alphabetic order
	public Iterable<Item> alphabetizedStack() {
		// Create a new stack
		Stack<Item> alphabetizedStack = new Stack<Item>();

		// Recursively traverse the BST in order and populate the stack
		populateStack(alphabetizedStack, this.root);

		// Return the alphabetized stack
		return alphabetizedStack;
	}

	// Recursively populate the stack
	private void populateStack(Stack<Item> stack, Node currentNode) {
		// If the current node is null, return
		if (currentNode == null) {
			return;
		}

		// Follow in order traversal of the BST
		populateStack(stack, currentNode.leftChild);
		stack.push(currentNode.value);
		populateStack(stack, currentNode.rightChild);
	}  
	
}
